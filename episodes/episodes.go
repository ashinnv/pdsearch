package episodesDBP

import (
	"fmt"
	"os"
	"strings"
	"time"
)

type TimeFrame struct { //STart time, end time
	Start time.Duration
	End   time.Duration
}

type Script struct {
	Lines []string
	Title string
}

type ScriptCache map[string]Script

type Episode map[TimeFrame]string //Store text between two different timestamps

type Library map[string]Episode //map[episode-name]episode

func (lib Library) Consume(scr Script) {
	if _, ok := lib[scr.Title]; !ok {
		lib[scr.Title] = scr.Lines
	} else {
		fmt.Println("Replacing extant script in scr:", scr.Title)
		lib[scr.Title] = scr.Lines
	}
}

// Turn a transcription file to an Episode data type
func ConsumeScript(fDat string) (Episode, error) {

	ep := make(Episode)
	/*
		fDat, fileErr := ioutil.ReadFile(targFile)
		if fileErr != nil {
			fmt.Println(fileErr)
			panic(fileErr)
			return Episode{}, fileErr
		}
	*/

	lines := strings.Split(string(fDat), "\n")
	for _, line := range lines {
		line = strings.ReplaceAll(line, "[Music]", "Music")
		ky, kyerr := genTFKey(line)
		if kyerr != nil {
			//panic(kyerr)
			fmt.Println("kyerr:", kyerr)
		}

		ep[ky] = genTxtLineVal(line)
	}

	return ep, nil

}

// Snip out the text line for that timeframe key
func genTxtLineVal(input string) string {
	sp := strings.Split(input, "-->")

	if len(sp) != 2 {
		return "" //, fmt.Errorf("Error in genTxtLineVal. sp was %d parts long", len(sp))
	}

	return sp[1] //, nil

}

// Generate the struct that will act as time keys in the episode
func genTFKey(input string) (TimeFrame, error) {
	dumpRet := TimeFrame{}

	sp := strings.Split(input, "]")
	if len(sp) != 2 {
		return dumpRet, fmt.Errorf("mastersplit in genTFKey wasn't 2 long, instead we got %d. Original input was: %s. Split was: %s", len(sp), input, sp[0])
	}

	tSpl := strings.Split(sp[0], "-->") //(Input is: [00:00:17.240 --> 00:00:18.560])
	if len(tSpl) != 2 {
		return dumpRet, fmt.Errorf("TimeSplit in genTFKey wasn't 2 long. We got %d instead", len(tSpl))
	}

	st, sterr := toDet(tSpl[0])
	nd, nderr := toDet(tSpl[1])
	if sterr != nil || nderr != nil {
		fmt.Printf("Error with sterr or nderr: Sterr was %s, nderr was %s\n", sterr.Error(), nderr.Error())
	}

	return TimeFrame{st, nd}, nil
}

// turn the three part durations into a single Go time.Duration
func toDet(input string) (time.Duration, error) {
	sp := strings.Split(input, ":")
	if len(sp) != 3 {
		return 0 * time.Second, fmt.Errorf("toDet sp split was %d units long", len(sp))
	}

	hr, _ := time.ParseDuration(sp[0] + "h")
	mn, _ := time.ParseDuration(sp[1] + "m")
	sn, _ := time.ParseDuration(sp[2] + "s")

	retDet := hr + mn + sn
	return retDet, nil
}

// LoadScript takes a file name and reads it into a Script
func LoadScript(fName string) Script {
	fDat, rfErr := os.ReadFile(fName)
	if rfErr != nil {
		fmt.Println(rfErr)
	}

	scriptLines := strings.Split(string(fDat), "\n")

	return Script{
		scriptLines,
		fName,
	}

}
