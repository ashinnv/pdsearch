module gitlab.com/ashinnv/dbPodcast

go 1.19

replace gitlab.com/ashinnv/episodesDBP => ../episodes

require gitlab.com/ashinnv/episodesDBP v1.1.1
