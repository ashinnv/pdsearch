package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/ashinnv/episodesDBP"
)

// "strings"

func main() {

	searchTerm := "room"
	epilib := episodesDBP.Library{}
	/*
		fDat, ioerr := ioutil.ReadFile("medOutput.txt")
		if ioerr != nil {
			fmt.Println("Error for reading file:", ioerr)
		}
	*/

	files, globErr := filepath.Glob("scripts/*.dbsc")
	if globErr != nil {
		fmt.Println("Glob Error:", globErr)
	}

	for file := range files {
		epilib.Consume(episodesDBP.ConsumeScript(file))
	}

	myLib, conerr := episodesDBP.ConsumeFile("medOutput.txt")
	if conerr != nil {
		fmt.Println("conerr:", conerr)
		os.Exit(0)
	}

	fmt.Println("Mylib len:", len(myLib))

	for k, v := range myLib {
		if strings.Contains(v, searchTerm) {
			fmt.Println("\n\n\n")
			fmt.Println(">", k, v)
			fmt.Println("\n\n\n")
		}
	}

}
